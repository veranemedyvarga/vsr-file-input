import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';


/**
 * `vsr-file-input`
 * 
 *
 * @customElement
 * @polymer
 */
class VsrFileInput extends PolymerElement {

  constructor() {
    super();
    this.file;
    this.filename;
    this.acceptableExtensionArray = [];
  }
  
  ready() {
    super.ready();
    this.addEventListener('dragenter', this._dragHandler, false);
    this.addEventListener('dragover', this._dragHandler, false);
    this.addEventListener('dragleave', this._unhighlight, false);
    this.addEventListener('drop', this._dropHandler, false);
  }

  static get properties() {
    return {
      file: {
        notify: true,
      },
      accept: {
        type: String,
        observer: '_getAcceptableExtensions'
      }
    }
  }

  _getAcceptableExtensions(newVal, oldVal) {
    this.acceptableExtensionArray = newVal.split(',');
  }

  _highlight() {
    this.$.dropzone.classList.add('highlighted');
  }

  _unhighlight() {
    this.$.dropzone.classList.remove('highlighted');
  }

  _checkIfAcceptableFile(item) {
    if (!this.acceptableExtensionArray.length || this.acceptableExtensionArray.indexOf(item[0].type) > -1) {
      return true;
    }
    this.dispatchEvent(new CustomEvent('vsr-file-error', {extension: item[0].type}));
    return false;
  }

  _dragHandler(e) {
    this._highlight();
    e.stopPropagation();
    e.preventDefault();
  }
  
  _dropHandler(e) {
    this._unhighlight();
    e.stopPropagation();
    e.preventDefault();
    let dt = e.dataTransfer;
    let files = dt.files;
    let items = dt.items;
    if (this._checkIfAcceptableFile(items)) {
      this.handleFileList(files);
    };
  }
  
  _handleFileInput(e) {
    if (this._checkIfAcceptableFile(e.target.files)) {
      this.handleFileList(e.target.files);
    }
  }
  
  handleFileList(filelist) {
    let file = filelist.item(0);
    this.file = file;
    this.filename = file.name;
  }
  
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          max-width: 600px;
        }
        #dropzone, #inputcontainer {
          display: flex;
          align-items: center;
          justify-content: center;
        }
        #inputcontainer {
          margin-top: 20px;
        }
        #dropzone {
          width: 100%;
          height: 200px;
          border: 2px dashed #dcdcdc;
          border-radius: 20px;
          flex-direction: column;
        }
        #dropzone.highlighted {
          border: 2px dashed #afacd6;
        }
        span {
          display: block;
          padding-right: 5px;
        }
        label {
          background-color: #dcdcdc;
          cursor: pointer;
          display: inline-block;
          height: 25px;
          line-height: 24px;
          text-align: center;
          width: 80px;
          padding: 5px;
          border-radius: 5px;
        }
        input {
          height: 0.1px;
          opacity: 0;
          overflow: hidden;
          position: absolute;
          width: 0.1px;
          z-index: -1;
        }
        #filename {
           height: 18px;
           margin-top: 10px;
        }
      </style>
      <form files="{{files}}">
        <div id="dropzone" url="[[url]]" on-drag-enter="_dragHandler" on-drag-over="_dragHandler" on-drop="_dropHandler" on-drag-leave="_unhighlight">
          <div id="inputcontainer">
            <span>Drop files here or</span>
            <input id="fileinput" type="file" on-change="_handleFileInput" accept="[[accept]]"/>
            <label for="fileinput" id="fileinputlabel">Choose file</label>
          </div>
          <div id="filename" file-name="[[filename]]">
            <span>{{filename}}</span>
          </div>
        </div>
      </form>
      `;
  }

}

window.customElements.define('vsr-file-input', VsrFileInput);
